# GrimoireLab Workshop

Some materials for "Soft Dive Into GrimoireLab" workshop

* [GrimoireLab deployment](#grimoirelab-deployment)
  * [Terraform deployment](#terraform-deployment)
    * [Requirements](#requirements-1)
    * [Deployment](#deployment)
  * [Ansible deployment](#ansible-deployment)
    * [Requirements](#requirements)
    * [Deploy and run](#deploy-and-run)
* [Python scripts for some GrimoireLab components](#python-scripts-for-some-grimoireLab-components)
  * [To run them in your computer](#to-run-them-in-your-computer)
    * [How to get your personal tokens](#how-to-get-your-personal-tokens)
* [Slides](#slides)
* [License](#license)

# GrimoireLab deployment

You can install GrimoireLab in your own machine using existing 
[analytics-demo](https://gitlab.com/Bitergia/lab/analytics-demo) project. But
included in this workshop repository, under the [provision](provision/) directory, 
there are means to help you to set and deploy GrimoireLab in cloud providers, like:
* Terraform recipes to set up basic infra in a cloud provider
* Ansible playbooks to deploy and udpate GrimoireLab on available machines

## Terraform deployment

[provider.tf](provision/provider.tf) file allows you to set up the basic infrastructure
to run GrimoireLab in a Digital Ocean droplet using Terraform.

Since Terraform can be used to configure infrastructure in other cloud providers,
I provide Digital Ocean example as reference for others willing to develop their
ones.

### Requirements

To test it, you need [Terraform](https://www.terraform.io/) installed in your
machine, and a [Digital Ocean](https://www.digitalocean.com/) account configured
to access via `ssh` and a [Digital Ocean Personal Access Token](https://www.digitalocean.com/docs/api/create-personal-access-token/).

You need your [ssh fingerprint](https://en.wikipedia.org/wiki/Public_key_fingerprint). To get it, execute:
```
$ ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub | awk '{print $2}'
MD5:c3:dc:9a:80:4e:80:0b:7c:7c:f6:5a:40:07:05:cd:09
```

So, your `FINGERPRINT` would be: `c3:dc:9a:80:4e:80:0b:7c:7c:f6:5a:40:07:05:cd:09`

### Deployment

From the `provision` directory, to initialize Terraform:
```
$ terraform init
```

To set up a droplet with name `<NAME>` you execute:

```
$ terraform apply -var name=<NAME> -var do_token=<DIGITALOCEAN_TOKEN> -var ssh_fingerprint=<FINGERPRINT> -auto-approve
...
```

Once finished, you should be able to connect via ssh to the <PUBLIC_IP> of your
droplet: 
```
@ ssh root@<PUBLIC_IP>
...
root@<NAME>:~#
```

Remember, `<NAME>` is the name you gave to this droplet when you set up with
Terraform.

Check that `analytics-demo` is already there:
```
root@<NAME>:~# ls
analytics-demo
root@<NAME>:~# cd analytics-demo
root@<NAME>:~/analytics-demo# ls -l
total 312
-rw-r--r-- 1 root root  35129 Sep 10 15:35 LICENSE
-rw-r--r-- 1 root root   3082 Sep 10 15:35 README.md
-rw-r--r-- 1 root root   3896 Sep 10 15:35 aliases.json
-rw-r--r-- 1 root root    856 Sep 10 15:35 apache-hatstall.conf
-rw-r--r-- 1 root root   1716 Sep 10 15:35 docker-compose.yml
-rw-r--r-- 1 root root 247109 Sep 10 15:35 orgs_file
-rw-r--r-- 1 root root    132 Sep 10 15:35 projects.json
-rw-r--r-- 1 root root   3255 Sep 10 15:35 setup.cfg
-rw-r--r-- 1 root root     62 Sep 10 15:35 shdb.cfg
```

So, you are ready to go with the rest of the workshop ;-)

## Ansible deployment

Ansible playbooks allow you to deploy and launch GrimoireLab in a remote server
using the configuration files in [config_files](config_files/).

### Requirements

To test them, you need [Ansible](https://www.ansible.com/) installed in your 
machine and one or several host machines with [Docker](https://www.docker.com/) and *some* 
amount of memory and HDD space.

The hosts shoud be included in your `/etc/ansible/hosts`. Edit it and add:

```
[grimoirelab]
HOST_IP

# The host should have Python3
[grimoirelab:vars]
ansible_python_interpreter=/usr/bin/python3
```

### Deploy and run

To deploy and run GrimoireLab in such hosts, you need to run the playbook
from the `ansible` directory:
```
~/grimoirelab-workshop/ansible $ ansible-playbook grimoirelab_deploy.yml
```

To update configuration files (saved in [config_files](config_files/)) and restart
GrimoireLab:
```
~/grimoirelab-workshop/ansible $ ansible-playbook grimoirelab_restart.yml
```

# Python scripts for some GrimoireLab components

* [Perceval 1st example](perceval-0.ipynb) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jsmanrique%2Fgrimoirelab-workshop/master?filepath=perceval-0.ipynb)
* [Perceval 2nd example](perceval-1.ipynb) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jsmanrique%2Fgrimoirelab-workshop/master?filepath=perceval-1.ipynb)
* [SortingHat 1st example](sortinghat-0.ipynb)
* [Kidash 1st example](kidash-0.ipynb)

Some might be ran directly from this page using [MyBinder](https://mybinder.org) (check the `launch binder` link), and others need to be ran locally in your computer.

## To run them in your computer

Requirements:
* [Jupyter notebook](https://jupyter.org/)

Install and set up the environment:

```
$ git clone https://gitlab.com/jsmanrique/grimoirelab-workshop
$ python3 -m venv grimoirelab-workshop
$ source grimoirelab-workshop/bin/activate
(grimoirelab-workshop) $ pip install -r requirements.txt
(grimoirelab-workshop) $ python -m install ipykernel --user --name=grimoirelab-workshop
```

Some examples might need credentials to access certain servicers. You can use a file to store your credentials and tokens. 
Create in the cloned directory a file called `credentials.yml` with the following format:

```
github: YOUR GITHUB TOKEN
gitlab: YOUR GITLAB TOKEN
meetup: YOUR MEETUP TOKEN
```

### How to get your personal tokens

To grant access to certain data sources, you might need an *access token* or *API key*. Here is how to set up some of them:

* [GitHub](https://help.github.com/en/articles/creating-a-personal-access-token-for-the-command-line)
* [GitLab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
* [Meetup](https://secure.meetup.com/meetup_api/key/)
* [Slack](https://get.slack.help/hc/en-us/articles/215770388-Create-and-regenerate-API-tokens)

# Slides

[Workshop-GrimoireLab-Slides.pdf](https://speakerdeck.com/bitergia/grimoirelab-intro-workshop-slides)

# License

[GPLv3](LICENSE)
