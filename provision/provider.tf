variable "do_token" {
  description = "Digital Ocean personal token"
  default = ""
}
variable "pub_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "pvt_key" {
  default = "~/.ssh/id_rsa"
}

variable "ssh_fingerprint" {
  default = ""
}

variable "name" {
  default = "demo-infrastructure"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "demos" {
  name = "${var.name}"
  image = "docker-18-04"
  size = "8gb"
  region = "nyc1"
  ssh_keys = [
    "${var.ssh_fingerprint}"
  ]

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo apt update",
      "sudo apt-get -y install python-minimal",
      "sudo sysctl -w vm.max_map_count=262144",
      "git clone https://gitlab.com/Bitergia/lab/analytics-demo.git"
    ]

    connection {
      host = "${digitalocean_droplet.demos.ipv4_address}"
      type = "ssh"
      user = "root"
      timeout = "2m"
    }
  }

  # provisioner "local-exec" {
  #   command = "ansible-playbook -i ${digitalocean_droplet.demos.ipv4_address}, --private-key ${file(var.pvt_key)} grimoirelab_deploy.yml"
  # }

}
